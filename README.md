# Playing with rxjs

This is a repo that's just me playing around with rxjs to familiarize myself with its ideas.

---

## Usage

```
git clone https://gitlab.com/japorized/play-with-rxjs
cd play-with-rxjs
npm install
```

To check the output of any of the files,

```
node examples/<file>.js
```

---

## References

* [RxViz](https://rxviz.com/) - pretty heavily used as my starter example source
