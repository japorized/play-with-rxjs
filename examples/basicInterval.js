const { interval } = require('rxjs');
const { take } = require('rxjs/operators');

const source = interval(1000).pipe(take(4));

source.subscribe((x) => console.log(x));
