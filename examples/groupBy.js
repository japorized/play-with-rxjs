const { range } = require('rxjs');
const { groupBy } = require('rxjs/operators');

const source = range(1, 10).pipe(groupBy((n) => n % 2));

source.subscribe((x) =>
  x.subscribe((y) => {
    console.log('Group', x.key);
    console.log(y);
  }),
);
