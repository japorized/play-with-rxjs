const { interval } = require('rxjs');
const { take, map, filter } = require('rxjs/operators');

// This is a messy array with a mix of numbers, strings, and strings of numbers
const values = ['9', 9, 'fun', '2', 'no', '10', 1];

// For fun, let's make the interval between each recognition random for each different run
const source = interval(Math.random() * 1000).pipe(
  take(values.length),
  map((i) => parseInt(values[i], 10)),
  filter((i) => !isNaN(i)),
);

source.subscribe((x) => console.log(x));
