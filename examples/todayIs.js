const { of, interval, range, EMPTY } = require('rxjs');
const { delay, take, map, concatMap } = require('rxjs/operators');

const sentence = new Date().toString().toUpperCase();
const words = sentence.split(' ');
const delayMS = 1000;

const wordDelay = (i) =>
  i === 0 ? delayMS : (words[i - 1].length + 1) * delayMS;

const wordStart = (i) =>
  i < words.length
    ? of(i).pipe(delay(wordDelay(i)))
    : EMPTY.pipe(delay(wordDelay(i)));

const wordObservable = (word) => {
  const letters = word.split('');

  return interval(delayMS).pipe(
    take(letters.length),
    map((i) => letters[i]),
  );
};

const source = range(0, words.length + 1).pipe(
  concatMap(wordStart),
  map((i) => wordObservable(words[i])),
);

source.subscribe((x) => console.log(x));
