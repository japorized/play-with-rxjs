const { Observable } = require('rxjs');

console.log(
  '# Returning squares of all non-zero natural numbers, failing at a random chance',
);
const source = Observable.create((observer) => {
  let n = 1;

  const interval = setInterval(() => {
    if (Math.random() < 0.8 && n < 9) {
      observer.next(n * n);
      n += 1;
    } else {
      observer.error('Oh no...');
    }
  }, 1000);

  return () => clearInterval(interval);
});

source.subscribe((x) => console.log(x));
